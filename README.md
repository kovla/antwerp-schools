
# Gemiddelde afstand tot kleuterscholen in Antwerpen

![alt text](distance3_grid_discrete.png "Gemiddelde afstand Antwerpse kleuterscholen")

# Samenvatting in het Nederlands

De brochure van het Antwerpse aanmeldingssysteem voor kleuteronderwijs (http://meldjeaan.antwerpen.be) zegt: "De bedoeling is om iedereen evenveel kansen te geven om zijn kind in te schrijven in een school van zijn keuze en wachtrijen aan de school te vermijden." (p. 3). Het kan zijn dat de tweede doelstelling bereikt wordt. De meeste kinderen worden immers in een school van keuze geplaatst. De eerste doelstelling is echter waar het schoentje wringt. 

De afstand tussen het officiële adres van het kind en het schooladres is een van de belangrijkste factoren die de toewijzing van plaatsen beïnvloeden. Het is echter zo dat sommige wijken gemiddeld genomen verder afgelegen zijn van scholen dan andere. Hierdoor worden de bewoners van deze wijken benadeeld in het huidige systeem, zowel bij het verdelen van vrije plaatsen als bij het opstellen van de wachtlijsten. Om een voorbeeld te geven, de gemiddelde afstand tot de drie het meest nabije scholen voor iemand op het Eilandje is 850m. Voor ouders die op de Meir wonen, wordt dit 240m, en 200m voor iemand op het Zuid. Gemiddeld genomen en op lange termijn zullen de bewoners van het Eilandje een lagere kans hebben op een plaats in een school van hun voorkeur. De kaart hierboven geeft de afstandsinformatie voor de hele stad weer. 

Het kan dus zijn dat het systeem werkt. Maar het werkt op een manier die bepaalde wijken discrimineert, want de verschillen zijn redelijk groot. De oplossing? Eenvoudig en vooral (bijna) gratis. Er moet een weging komen die de gemiddelde afstand tot enkele (bijvoorbeeld 3-5) het meest nabije scholen zou weerspiegelen. Deze gewogen afstand moet als basis dienen voor de toewijzingsprocedure. Een simpele aritmetische operatie dus in ruil voor een eerlijker systeem. 


# Project description

In the city of Antwerp, places in kindergartens are assigned based on an online enrollment system. One of the most important criteria for that system is the distance between the official address of the child and that of the school. One of the problems with this approach is that some areas of the city are situated further on the average from schools than others. Given the importance of the distance criterion, this introduces a discriminatory bias for those areas. While the system may work in general, that systemic bias ensures that certain areas are at a disadvantage in the long run. 

The visualization above depicts the average distance to three closest schools for the entire city. 


# Methods

Programming language `R` was used to create the map, with packages `ggplot2` and `ggmap`. The list of school addresses (kindergartens or *kleuterscholen*) was obtained from the website of the enrollment system (http://meldjeaan.antwerpen.be). Schools for seamen and schools of the network *Joods onderwijs* were not considered, since these are geared towards their respective target groups. The address list was converted to geodata (latitude and longitude), which allowed to calculate the mean distance to three closest school for each spot in the city of Antwerp. Subsequently, this information was plotted as a heatmap that you can see above. Program code for this project is public for the reasons of transparency and is available in the source section. 

# Results

The map demonstrates, that even though school coverage is good enough for most city areas, some areas (like Eilandje) are situated much further than more central neighborhoods. This implies that in the current distribution system of free places in schools, these areas will be discriminated in the long run. It is possible that children from this area will still find place at the preferred schools, but the probability of for those children will be lower. In addition, even for areas with good coverage, there exist substantial differences in the mean distance.  

A possible solution is very simple and requires little (if any) funds. All that needs to be done is a weighting of the actual distance by the factor that reflects the mean distance to several (e.g. 3-5) closest schools. That weighted distance would be then fed into the algorithm, which would result in a more equal distribution for all city areas. A simple arithmetic operation for a fairer system. 